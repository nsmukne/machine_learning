import tensorflow as tf
import numpy as np
    
class CNN(object):
    
    def assertions(self):
        assert len(self.cnn_poolsizes) == len(self.cnn_Wshapes), "Number of CNN Poolings and Weights must be equal!"
        assert (len(self.ann_dims)+1) == len(self.ann_activations), "Number of FC Dimensions and Activations must be equal!"
    
    def __init__(self, cnn_poolsizes, cnn_Wshapes, ann_activations, lr, epochs, print_step, batch_size,  ann_dims, moment = .9, loss='cross-entropy', optimize_method='rmsp', regularization = None, cnn_padding='SAME'):
        self.ann_activations = ann_activations + ['softmax']
        self.lr = lr
        self.moment = moment
        self.loss = loss
        self.print_step = print_step
        self.batch_size = batch_size
        self.epochs = epochs
        self.cnn_poolsizes = cnn_poolsizes
        self.cnn_Wshapes = cnn_Wshapes
        self.cnn_padding = cnn_padding
        self.ann_dims = ann_dims
        self.optimize_method = optimize_method
        if regularization is not None:
            self.regularization_method = regularization[0]
            self.regularization_penalty = regularization[1]
        else:
            self.regularization_method = None
            self.regularization_penalty = None
        self.cnn_weights = []
        self.cnn_biases = []
        self.ann_weights = []
        self.ann_biases = []
        self.assertions()
    
    
    def reset_weights_biases(self):
        self.cnn_weights = []
        self.cnn_biases = []
        self.ann_weights = []
        self.ann_biases = []
    

    def add_conv_pool_layer(self, X, W, b, poolsize, padding_ = 'SAME'):
        conv_out = tf.nn.conv2d(X, W, strides=[1,1,1,1], padding=padding_)
        conv_out = tf.nn.bias_add(conv_out, b)
        pool_out = tf.nn.max_pool(conv_out, ksize=[1,poolsize[0], poolsize[1], 1], strides=[1, poolsize[0], poolsize[1],1], padding=padding_)
        return tf.nn.relu(pool_out)

    
    def add_fully_connected_layer(self, layer, w, b, activation):
        return self.activate((tf.matmul(layer, w) + b), activation)
        
    
    def new_weight(self, shape):
        return tf.Variable(tf.truncated_normal(shape, stddev=0.05))
    
    
    def new_bias(self, shape):
        return tf.Variable(tf.zeros(shape))
        
    
    def flatten_conv_layer(self, conv_layer):
        conv_layer_shape = conv_layer.get_shape().as_list()
        # flattening data for ANN
        flatten_conv_layer = tf.reshape(conv_layer, [conv_layer_shape[0], np.prod(conv_layer_shape[1:])])
        return flatten_conv_layer
    
    
    def fit(self, train, labels):
        self.reset_weights_biases()
        self.fit_run(train, labels)
    
    
    def fit_transform(self, train, labels):
        self.fit_run(train, labels)
        return self.transform(train)
    
    
    def fit_run(self, train, labels):
        tf.reset_default_graph()
        
        cnn_weights = []
        cnn_biases = []
        ann_weights = []
        ann_biases = []

        # defining tf Variables
        X = tf.placeholder(shape = (self.batch_size, train.shape[1], train.shape[2], train.shape[3]), dtype=tf.float32, name = 'X')
        y = tf.placeholder(shape = (self.batch_size, labels.shape[1]), dtype=tf.float32, name = 'y')

        for cnn_Wshape, cnn_poolsize in zip(self.cnn_Wshapes, self.cnn_poolsizes):
            cnn_Winit = self.new_weight(cnn_Wshape)
            cnn_weights.append(cnn_Winit)
            cnn_binit = self.new_bias(cnn_Wshape[-1])
            cnn_biases.append(cnn_binit)
            
        # Convolutions and MaxPoolings
        Z = X
        for itr in range(len(cnn_weights)):
            Z = self.add_conv_pool_layer(Z, cnn_weights[itr], cnn_biases[itr], self.cnn_poolsizes[itr], self.cnn_padding)
            
        
        # initializing feedforward weights
        current_dim = np.prod(Z.get_shape().as_list()[1:])
        final_dim = labels.shape[1]

        for ann_dim in self.ann_dims + [final_dim]:
            ann_Winit = self.new_weight(shape = (current_dim, ann_dim))
            ann_weights.append(ann_Winit)
            ann_binit = self.new_bias(ann_dim)
            ann_biases.append(ann_binit)
            current_dim = ann_dim

        # flattening data for ANN
        Z = self.flatten_conv_layer(Z)

        # FFN part
        # The final softmax layer is also applied
        for itr in range(len(ann_weights)):
            Z = self.add_fully_connected_layer(Z, ann_weights[itr], ann_biases[itr], self.ann_activations[itr])
            if self.regularization_method == 'dropout':
                # Dropouts are only applied to Fully Connected Layer, due to computation limitations
                Z = tf.nn.dropout(Z, noise_shape = (1, int(Z.get_shape()[1])), keep_prob = (1-self.regularization_penalty))
        
        prediction = Z
        
        # defining cost
        if self.loss == 'rmse':
            cost = tf.sqrt(tf.reduce_mean(tf.square(tf.sub(y, prediction))))
        elif self.loss == 'cross-entropy':
            cost=-tf.reduce_sum(y*tf.log(tf.clip_by_value(prediction,1e-10,1.0)))
        
        # Regularizer
        cost = self.regularize(cost, cnn_weights + ann_weights, self.regularization_method, self.regularization_penalty)
            
        
        # defining train function
        train_step = self.define_train_step(self.lr, cost, self.optimize_method)
        
        init = tf.global_variables_initializer()
        with tf.Session() as sess:
            sess.run(init)
            for i in range(self.epochs):
                samp = np.random.choice(range(train.shape[0]), self.batch_size, replace = False)
                Xbatch = train[samp,]
                Ybatch = labels[samp]
                sess.run(train_step, feed_dict = {X: Xbatch, y: Ybatch})
                if i % self.print_step == 0:
                    print ('Step ', i, 'Mini Batch Error: ',sess.run(cost, feed_dict = {X: Xbatch, y: Ybatch}))
        
            # getting the weights, biases for transformations
            for idx in range(len(cnn_weights)):
                self.cnn_weights.append(sess.run(cnn_weights[idx]))
                self.cnn_biases.append(sess.run(cnn_biases[idx]))

            for idx in range(len(ann_weights)):
                self.ann_weights.append(sess.run(ann_weights[idx]))
                self.ann_biases.append(sess.run(ann_biases[idx]))
    
    
    def transform(self, X):
        tf.reset_default_graph()
        sess = tf.Session()
        Z = tf.constant(X, dtype=tf.float32)
        for itr in range(len(self.cnn_weights)):
            Z = self.add_conv_pool_layer(Z, self.cnn_weights[itr], self.cnn_biases[itr], self.cnn_poolsizes[itr])
        
        # flattening data for ANN
        Z = self.flatten_conv_layer(Z)

        # ANN part
        for itr in range(len(self.ann_weights)):
            Z = self.add_fully_connected_layer(Z, self.ann_weights[itr], self.ann_biases[itr], self.ann_activations[itr])
        
        
        prediction = tf.argmax(Z, 1)
        
        return prediction.eval(session=sess)
    
    
    def batch_transform(self, X, batch_size):
        max_size = X.shape[0]
        max_runs = int(max_size/batch_size)
        predictions = np.array([])
        for run in range(max_runs):
            # for handling condition when max_size%batch_size!=0
            if run == (max_runs-1):
                prediction = self.transform(X[batch_size*run:])
                predictions = np.concatenate((predictions, prediction))
                break
            # for batch sized data
            prediction = self.transform(X[batch_size*run:batch_size+batch_size*run])
            predictions = np.concatenate((predictions, prediction))
        return np.array(predictions)
    
    
    def activate(self, layer, activation):
        if activation == 'relu':
            return tf.nn.relu(layer)
        elif activation == 'sigmoid':
            return tf.nn.sigmoid(layer)
        elif activation == 'softmax':
            return tf.nn.softmax(layer)
        elif activation == 'tanh':
            return tf.nn.tanh(layer)
        
        
    def define_train_step(self, lr, cost, optimize_method = 'rmsp'):
        if self.optimize_method == 'gd':
            return tf.train.GradientDescentOptimizer(learning_rate = lr).minimize(cost)
        elif self.optimize_method == 'adag':
            return tf.train.AdagradOptimizer(learning_rate = lr).minimize(cost)
        elif self.optimize_method == 'mom':
            return tf.train.MomentumOptimizer(learning_rate = lr, momentum = self.moment).minimize(cost)
        elif self.optimize_method == 'adam':
            return tf.train.AdamOptimizer(learning_rate = lr).minimize(cost)
        elif self.optimize_method == 'ftrl':
            return tf.train.FtrlOptimizer(learning_rate = lr).minimize(cost)
        elif self.optimize_method == 'rmsp':
            return tf.train.RMSPropOptimizer(learning_rate = lr).minimize(cost)
        
        
    def regularize(self, cost, weights, regularization_method, regularization_penalty):
        if self.regularization_method == 'l2':
            for w in weights:
                cost += self.regularization_penalty*tf.nn.l2_loss(w)
            return cost
        elif self.regularization_method == 'l1':
            for w in weights:
                cost += self.regularization_penalty*tf.reduce_sum(tf.abs(w))
            return cost
        else:
            # dropouts are handled in earlier part of defining the prediction
            # Therefore, do nothing if dropouts or None
            return cost
        