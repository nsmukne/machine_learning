from collections import Counter
import numpy as np
import pos_tag_utils as pt_utils


class grammar_learn(object):
    def __init__(self, training_sentences, lh_window, target_word=None, end_tag=None, min_rule_size=4, tags_count_dev=1.5,
                 tag_idf_dev=-1.5, tag_position_score_dev=-1):
        self.training_sentences = training_sentences
        self.target_word = target_word
        self.lh_window = lh_window  # look ahead win
        self.end_tag = end_tag  # rename to self.max_rule_len

        self.location_index_dict = None
        self.index_location_dict = None
        self.sequences = None
        self.training_sequences = None
        self.pos_tag_sequences = None
        self.M = None
        self.transition_mat_sequential = None
        self.transition_mat_positional = None
        self.no_of_tags = None
        self.rule = None
        self.mat = None
        self.pos_tf_dict = None
        self.top_pos_tags_dict = None
        self.tag_positional_prob_dict = None
        self.idf_lower_threshold = None
        self.no_of_tags = None

        self.tags_count_dev = tags_count_dev
        self.tag_idf_dev = tag_idf_dev
        self.tag_position_score_dict = None

        self.min_rule_size = min_rule_size
        self.max_rule_size = lh_window
        self.grammar_rule = None
        self.combined_rule = None
        self.seq_rule_dict = None
        self.tag_position_score_dev = tag_position_score_dev

        self.prediction = None

    def _build_sequences(self):
        self.pos_tag_sequences = [pt_utils.pos_tag_sentence(i) for i in
                                  self.training_sentences]  # rename to tagged sentences
        self.training_sequences = [[j[1] for j in i] for i in self.pos_tag_sequences]

    def _build_location_dicts(self):
        if self.target_word is not None:
            self.location_index_dict = {self.target_word.lower(): 0}
            self.index_location_dict = {0: self.target_word.lower()}  # inverse dict
        for seq in [[j[1] for j in i] for i in self.pos_tag_sequences]:
            for word in seq:
                if word not in self.location_index_dict:
                    self.location_index_dict[word] = len(self.location_index_dict)
                    self.index_location_dict[self.location_index_dict[word]] = word
        # lh_window = 30 # look ahead window
        self.M = len(self.location_index_dict)  # number of states/locations, with start
        self.no_of_tags = len(self.location_index_dict) - 1

    def _build_transition_matrices(self):
        self.transition_mat_sequential = np.zeros((self.M, self.M))
        for seq in self.training_sequences:
            if self.target_word is not None:
                seq = [self.target_word.lower()] + seq[1:]
            if len(seq) > 1:
                for idx in range(len(seq) - 1):
                    self.transition_mat_sequential[
                        self.location_index_dict[seq[idx]], self.location_index_dict[seq[idx + 1]]] += 1
        self.transition_mat_positional = np.zeros((self.M, self.lh_window))
        for seq in self.training_sequences:
            if self.target_word is not None:
                seq = [self.target_word.lower()] + seq[1:]
            for idx in range(len(seq)):
                if idx < self.lh_window:
                    self.transition_mat_positional[self.location_index_dict[seq[idx]], idx] += 1

    def _build_term_freq_dicts(self):
        self.mat = np.zeros((self.no_of_tags, self.lh_window))
        # mat_counts=np.zeros((no_of_tags,lh_window))
        self.pos_tf_dict = Counter({})
        self.top_pos_tags_dict = {}
        self.tag_positional_prob_dict = {}  ###
        for position in range(0, self.lh_window):
            pos_indexes = self.transition_mat_positional[:, position].argsort()[::-1][
                          :self.no_of_tags]  # top tags' indexes @ position
            top_pos_tags = [self.index_location_dict[pos_index] for pos_index in pos_indexes]  # top tags @ position
            top_tag_values = np.sort(self.transition_mat_positional[:, position])[::-1][
                             :self.no_of_tags]  # counts @ position of the tags used for IDF
            self.tag_positional_prob_dict[position] = dict(zip(top_pos_tags, [i / top_tag_values.sum() for i in
                                                                              top_tag_values]))  # normalized counts of tags at each position- prob. of tag at position dict
            # mat stores arg sorted indexes of tags @ position
            self.mat[:, position] = pos_indexes  # np.sort(transition_mat_positional[:,position])[::-1][:gmr_rule_width]
            # mat_counts stores sorted values of tags @ position

            try:
                mat_counts = np.sort(self.transition_mat_positional[:, position])[::-1][:self.no_of_tags]
                # filtered tags- if greater than std. devs.
                top_n = len(
                    [i for i in mat_counts if i > (np.std(mat_counts) * self.tags_count_dev + np.mean(mat_counts))])
                # taking only the filtered tags in dict
                self.top_pos_tags_dict[position] = top_pos_tags[:top_n]
            except:
                self.top_pos_tags_dict[position] = []

            # pos tags term freq dict - keep updating values over positions - to address the overall effect of tags --UNUSED
            self.pos_tf_dict = self.pos_tf_dict + Counter(dict(zip(top_pos_tags, top_tag_values)))

        ctr_temp = Counter({})
        for pos in self.top_pos_tags_dict:
            ctr_temp = ctr_temp + Counter(self.top_pos_tags_dict[pos])
        # building idf dict
        self.tag_idf_dict = {}
        for tag in ctr_temp:
            self.tag_idf_dict[tag] = ctr_temp[tag] * np.log(float(len(self.top_pos_tags_dict)) / ctr_temp[tag])
        # sorted(tag_idf_dict.items(), reverse=True, key=lambda a:a[1])
        self.idf_lower_threshold = np.mean(self.tag_idf_dict.values()) + np.std(
            self.tag_idf_dict.values()) * self.tag_idf_dev

    def _build_tag_position_score_dict(self):
        self.tag_position_score_dict = {}
        list_idx = 1
        while list_idx < len(self.top_pos_tags_dict):
            for tag1 in self.top_pos_tags_dict[list_idx - 1]:
                for tag2 in self.top_pos_tags_dict[list_idx]:
                    if (tag1 != tag2) and (self.tag_idf_dict[tag1] > self.idf_lower_threshold) and (self.tag_idf_dict[tag2] > self.idf_lower_threshold):
                        self.tag_position_score_dict['%d_%d_%s_%s' % (list_idx, list_idx + 1, tag1, tag2)] = \
                        self.tag_positional_prob_dict[list_idx - 1][tag1] * self.tag_positional_prob_dict[list_idx][
                            tag2] * self.transition_mat_sequential[
                            self.location_index_dict[tag1], self.location_index_dict[tag2]]
            list_idx += 1
        self.tag_position_score_dict_filtered = dict([i for i in self.tag_position_score_dict.items() if i[1] > np.mean(
            self.tag_position_score_dict.values()) + self.tag_position_score_dev * np.std(
            self.tag_position_score_dict.values())])

    def _build_grammar_rule(self):  # seq_len should be rule_len
        rule_dict = {}
        for k in sorted(self.tag_position_score_dict_filtered):
            rule_dict[re.split('(?<=\d)_(?=[A-Za-z\.\$\:\;])', k)[0]] = rule_dict.get(
                re.split('(?<=\d)_(?=[A-Za-z\.\$\:\;])', k)[0], []) + [re.split('(?<=\d)_(?=[A-Za-z\.\$\:\;])', k)[1]]

        for seq_key in rule_dict:
            rule_dict[seq_key] = [set(j) for j in zip(*[i.split('_') for i in rule_dict[seq_key]])]

        self.grammar_rule = ''  # '<%s>'%target_word
        position_tagex_dict = {i: set() for i in range(1, self.lh_window + 1)}
        for idx in range(1, self.lh_window):
            seq_key = '%d_%d' % (idx, idx + 1)
            if seq_key in rule_dict:
                position_tagex_dict[idx] = position_tagex_dict[idx].union(rule_dict[seq_key][0])
                position_tagex_dict[idx + 1] = position_tagex_dict[idx + 1].union(rule_dict[seq_key][1])
        if self.target_word is not None:
            position_tagex_dict[1] = position_tagex_dict[1].union([self.target_word])
        if self.end_tag is not None:
            position_tagex_dict[self.lh_window] = position_tagex_dict[self.lh_window].union([self.end_tag])

        for idx in position_tagex_dict:
            if len(position_tagex_dict[idx]) > 0:
                if (len(position_tagex_dict[idx]) == 1) and (idx not in [1, len(position_tagex_dict)]):
                    self.grammar_rule += '<.*>'
                else:
                    rule_temp = '|'.join(position_tagex_dict[idx])
                    rule_temp = '<%s>' % rule_temp
                    rule_temp = re.sub('(?<=[A-Z][A-Z])(?<!DA|AT)[A-Z]', '.', rule_temp)
                    self.grammar_rule += rule_temp
            else:
                self.grammar_rule += '<.*>'
        return self.grammar_rule

    def pre_train(self):
        self._build_sequences()
        self._build_location_dicts()

    def train_iteration(self):
        self._build_transition_matrices()
        self._build_term_freq_dicts()
        self._build_tag_position_score_dict()
        self._build_grammar_rule()

    def train(self):
        self.seq_rule_dict = {}
        if not self.pos_tag_sequences:
            self.pre_train()
        for seq_len in range(self.min_rule_size, self.max_rule_size + 1):
            if self.end_tag is not None:
                self.training_sequences = [[j[1] for j in i] for i in self.pos_tag_sequences if len(i) >= seq_len]
                self.training_sequences = [i for i in self.training_sequences if i[seq_len - 1] == self.end_tag]
            self.lh_window = seq_len
            self.train_iteration()
            self.seq_rule_dict[seq_len] = self.grammar_rule
        self.grammar_rule = "\n".join(
            ['rule_%d:{%s}' % (i + 1, self.seq_rule_dict.values()[i]) for i in range(len(self.seq_rule_dict.values()))])

    def predict(self, testing_data):
        self.prediction = [pt_utils.chunker(pt_utils.pos_tag_sentence(i), self.grammar_rule) for i in testing_data]
        return self.prediction