import datefinder
import nltk
import re
from nltk.corpus import stopwords

cachedStopWords = stopwords.words("english")
# cachedStopWords.pop(cachedStopWords.index('not'))
# cachedStopWords.pop(cachedStopWords.index('no'))
# cachedStopWords.pop(cachedStopWords.index('with'))
df = datefinder.DateFinder()


def sub_pattern(text):
    text = re.sub(r'[\/\'\"\!\#\$\%\(\)\*\+\,\;\<\=\>\?\@\[\]\^\_\`\{\|}\~\:]', ' ', text)
    return text


def remove_non_strings(text):
    text = re.sub(r'[\'\"\!\#\$\%\(\)\*\+\,\;\<\=\>\?\@\[\]\^\`\{\|}\~\:\\].*?(?=\s)', '', text.encode('string-escape'))
    return text


def tokenize_sentence(sentence, remove_punct=False, remove_non_strs=False):
    if remove_punct:
        sentence = sub_pattern(sentence)
    if remove_non_strs:
        sentence = remove_non_strings(sentence)
    return nltk.word_tokenize(sentence.lower())


def pos_tag_sentence(sentence, remove_punct=False, remove_non_strs=False):
    sentence_tokenized = tokenize_sentence(sentence, remove_punct=remove_punct, remove_non_strs=remove_non_strs)
    return nltk.pos_tag(sentence_tokenized)


def remove_stop_words(text):
    cleaned_text = [word for word in text.split() if word not in cachedStopWords]
    return ' '.join(cleaned_text)


def clean_df_object(dt):
    if (len(dt[2]['digits']) == 2 and ('-' not in dt[2]['delimiters']) and (' ' not in dt[2]['delimiters'])) or \
            (len(dt[2]['digits']) > 0 and dt[2]['months']) or len(dt[2]['digits']) == 3:
        return True
    return False


def get_clean_dates(str_):
    valid_years_pattern = '199[0-9]|20[01][0-7]'
    years = re.findall(valid_years_pattern, str_)
    temp = list(df.extract_date_strings(str_))
    temp = [i for i in temp if clean_df_object(i)]
    dates = []
    if temp:
        dates_temp = [i[0] for i in temp]
        for dt in dates_temp:
            dates.append(re.sub('\s?on|\s?at', '', dt).strip())
    dates = dates + years
    return dates


def process_dates_in_str(str_):
    dates = get_clean_dates(str_)
    dates = sorted(dates, reverse=True, key=lambda a: len(a))
    tmp_str_ = ''
    if dates:
        for dt in dates:
            dt_ = dt.replace(' ', '_')
            str_ = re.sub('(?<!_)' + dt, 'DATE_' + dt_, str_)
    return str_


def insert_date_tag(pos_tagged_sentence):
    l = []
    for tup in pos_tagged_sentence:
        if get_clean_dates(tup[0]) or ('date' in tup[0]):
            tup = (tup[0], 'DATE')
        l.append(tup)
    return l


def exchange_tag(pos_tagged_sentence, tag_exchange_list):
    l = []
    for tup in pos_tagged_sentence:
        for exchange_tag in tag_exchange_list:
            if exchange_tag[0] == tup[0]:
                tup = (exchange_tag[0], exchange_tag[1])
        l.append(tup)
    return l


def pos_tag_sentence(sentence, process_date_str=True, tag_exchange_list=None):
    sentence = sub_pattern(sentence)
    sentence = remove_non_strings(sentence)
    if process_date_str:
        sentence = process_dates_in_str(sentence)
    sentence_tokenized = nltk.word_tokenize(sentence.lower())
    pos_tagged_sentence = nltk.pos_tag(sentence_tokenized)
    pos_tagged_sentence = insert_date_tag(pos_tagged_sentence)
    if isinstance(tag_exchange_list, list):
        pos_tagged_sentence = exchange_tag(pos_tagged_sentence, tag_exchange_list)
    return pos_tagged_sentence


def chunker(pos_tagged_sent, rule=r"""Chunk: {<JJ>*<NN\w?>+}""", loop=1):
    rule = re.sub('<(?=[A-Z])', '<.*?', rule)
    rule = re.sub('\|(?=[A-Z])', '|.*?', rule)
    rule = re.sub('(?<=[a-z])\|', '.*?|', rule)
    rule = re.sub('(?<=[a-z])>', '.*?>', rule)
    chunkParser = nltk.RegexpParser(rule, loop=loop)
    pos_tagged_sent = [(word_tag[0], word_tag[0] + '_' + word_tag[1]) for word_tag in pos_tagged_sent]
    pos_tagged_sent = [i for i in chunkParser.parse(pos_tagged_sent) if not isinstance(i, tuple)]
    tree_dict = {}
    for tree in pos_tagged_sent:
        leaves = [(i[0], i[1].split('_')[1] if 'date' not in i[1].split('_')[0] else 'DATE') for i in tree.leaves()]
        tree_dict[tree.label()] = tree_dict.get(tree.label(), []) + [leaves]
    return tree_dict


def generate_target_sentences(target_word, sentences, right_window=10, left_window=10, num_sents=1500):
    target_parts = []
    for target_sent in sentences:
        target_sent = sub_pattern(target_sent)
        target_sent = re.sub('\s{2,}', ' ', target_sent).lower()
        target_sent = remove_non_strings(target_sent)
        if left_window > 0:
            target_parts = target_parts + re.findall(r'\S*\s' * left_window + target_word + '\s\S*' * right_window,
                                                     target_sent, re.IGNORECASE)
        else:
            target_parts = target_parts + re.findall(target_word + '\s\S*' * right_window, target_sent, re.IGNORECASE)
        if len(set(target_parts)) > num_sents:
            break
    return target_parts
