import tensorflow as tf
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
    
class DeepClassifier(object):
    
    def assertions(self):
        assert (len(self.dims)+1) == len(self.activations), "Number of FC Dimensions and Activations must be equal!"
    
    def __init__(self, activations, lr, epochs, print_step, batch_size,  dims, moment = .9, loss='cross-entropy', optimize_method='rmsp', regularization = None, dropout = 0):
        self.activations = activations + ['softmax']
        self.lr = lr
        self.moment = moment
        self.loss = loss
        self.print_step = print_step
        self.batch_size = batch_size
        self.epochs = epochs
        self.dims = dims
        self.optimize_method = optimize_method
        if regularization is not None:
            self.regularization_method = regularization[0]
            self.regularization_penalty = regularization[1]
        else:
            self.regularization_method = None
            self.regularization_penalty = None
        self.weights = []
        self.biases = []
        self.assertions()
        self.dropout = dropout
    
    
    def reset_weights_biases(self):
        self.weights = []
        self.biases = []

    
    def add_hidden_layer(self, layer, w, b, activation):
        return self.activate((tf.matmul(layer, w) + b), activation)
        
    
    def new_weight(self, shape):
        return tf.Variable(tf.truncated_normal(shape, stddev=0.05))
    
    
    def new_bias(self, shape):
        return tf.Variable(tf.zeros(shape))
    
    
    def fit(self, train, labels):
        self.reset_weights_biases()
        self.fit_run(train, labels)
    
    
    def fit_transform(self, train, labels):
        self.fit_run(train, labels)
        return self.transform(train)
    
    
    def fit_run(self, train, labels):
        tf.reset_default_graph()
        
        weights = []
        biases = []

        # defining tf Variables
        X = tf.placeholder(shape = (self.batch_size, train.shape[1]), dtype=tf.float32, name = 'X')
        y = tf.placeholder(shape = (self.batch_size, labels.shape[1]), dtype=tf.float32, name = 'y')

        # initializing weights
        current_dim = train.shape[1]
        final_dim = labels.shape[1]

        for dim in self.dims + [final_dim]:
            Winit = self.new_weight(shape = (current_dim, dim))
            weights.append(Winit)
            binit = self.new_bias(dim)
            biases.append(binit)
            current_dim = dim

        Z = X
        # The final softmax layer is also applied
        for itr in range(len(weights)):
            Z = self.add_hidden_layer(Z, weights[itr], biases[itr], self.activations[itr])
            if (self.dropout > 0) and (itr < len(weights) - 1):
                # Dropouts are only applied to Fully Connected Layer, due to computation limitations
                Z = tf.nn.dropout(Z, noise_shape = (1, int(Z.get_shape()[1])), keep_prob = (1-self.dropout))
        
        prediction = Z
        
        # defining cost
        if self.loss == 'rmse':
            cost = tf.sqrt(tf.reduce_mean(tf.square(tf.sub(y, prediction))))
        elif self.loss == 'cross-entropy':
            cost=-tf.reduce_sum(y*tf.log(tf.clip_by_value(prediction,1e-10,1.0)))
        
        # Regularizer
        cost = self.regularize(cost, weights, self.regularization_method, self.regularization_penalty)
            
        
        # defining train function
        train_step = self.define_train_step(self.lr, cost, self.optimize_method)
        
        init = tf.global_variables_initializer()
        with tf.Session() as sess:
            sess.run(init)
            for i in range(self.epochs):
                samp = np.random.choice(range(train.shape[0]), self.batch_size, replace = False)
                Xbatch = train[samp,]
                Ybatch = labels[samp]
                sess.run(train_step, feed_dict = {X: Xbatch, y: Ybatch})
                if i % self.print_step == 0:
                    print ('Step ', i, 'Mini Batch Error: {:.9f}'.format(sess.run(cost, feed_dict = {X: Xbatch, y: Ybatch})/self.batch_size))
        
            # getting the weights, biases for transformations

            for idx in range(len(weights)):
                self.weights.append(sess.run(weights[idx]))
                self.biases.append(sess.run(biases[idx]))
    
    
    def transform(self, X):
        tf.reset_default_graph()
        sess = tf.Session()
        Z = tf.constant(X, dtype=tf.float32)
        ##
        for itr in range(len(self.weights)):
            Z = self.add_hidden_layer(Z, self.weights[itr], self.biases[itr], self.activations[itr])
        ##
        prediction = tf.argmax(Z, 1)
        ##
        return prediction.eval(session=sess)
    
    
    def batch_transform(self, X, batch_size):
        max_size = X.shape[0]
        max_runs = int(max_size/batch_size)
        predictions = np.array([])
        for run in range(max_runs):
            # for handling condition when max_size%batch_size!=0
            if run == (max_runs-1):
                prediction = self.transform(X[batch_size*run:])
                predictions = np.concatenate((predictions, prediction))
                break
            # for batch sized data
            prediction = self.transform(X[batch_size*run:batch_size+batch_size*run])
            predictions = np.concatenate((predictions, prediction))
        return np.array(predictions)
    
    
    def activate(self, layer, activation):
        if activation == 'relu':
            return tf.nn.relu(layer)
        elif activation == 'sigmoid':
            return tf.nn.sigmoid(layer)
        elif activation == 'softmax':
            return tf.nn.softmax(layer)
        elif activation == 'tanh':
            return tf.nn.tanh(layer)
        
        
    def define_train_step(self, lr, cost, optimize_method = 'rmsp'):
        if self.optimize_method == 'gd':
            return tf.train.GradientDescentOptimizer(learning_rate = lr).minimize(cost)
        elif self.optimize_method == 'adag':
            return tf.train.AdagradOptimizer(learning_rate = lr).minimize(cost)
        elif self.optimize_method == 'mom':
            return tf.train.MomentumOptimizer(learning_rate = lr, momentum = self.moment).minimize(cost)
        elif self.optimize_method == 'adam':
            return tf.train.AdamOptimizer(learning_rate = lr).minimize(cost)
        elif self.optimize_method == 'ftrl':
            return tf.train.FtrlOptimizer(learning_rate = lr).minimize(cost)
        elif self.optimize_method == 'rmsp':
            return tf.train.RMSPropOptimizer(learning_rate = lr).minimize(cost)
        
        
    def regularize(self, cost, weights, regularization_method, regularization_penalty):
        if self.regularization_method == 'l2':
            for w in weights:
                cost += self.regularization_penalty*tf.nn.l2_loss(w)
            return cost
        elif self.regularization_method == 'l1':
            for w in weights:
                cost += self.regularization_penalty*tf.reduce_sum(tf.abs(w))
            return cost
        else:
            return cost
